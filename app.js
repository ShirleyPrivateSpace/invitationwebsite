// 최초의 진입점 같은 어플리케이션
// app : exppress에서 초입부 이름으로 권장함

var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer');
var _storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'public/images/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname);
  }
});
var upload = multer({ storage: _storage });

var app = express();
// bodyparser : post 방식으로 전달받은 요청 처리
app.use(bodyParser.urlencoded({
  extended: false
}));
//정적인 파일이 위치할 디렉토리(관습이름:public)를 지정함
app.use(express.static('public'));

var mysql = require('mysql');
var conn = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '111111',
  database: 'invitation'
});
conn.connect();
var how2goSrc = "";
var how2goDst = "";
var editingComment = 0;

// [1 : express 시작]
app.get('/', function(req, res) {
  res.redirect('home.html');
});

app.post('/uploadImage', upload.single('uploadPhoto'), function(req,res) {
  console.log(req.file);
  res.send({filename: req.file.filename});
});

app.post('/introRead', function(req, res) {
  var who = req.body.who;
  var index = 0;
  if (who == 'his')
    index = 1;
  else if (who == 'her')
    index = 2;

  var sql = 'SELECT * FROM we WHERE id=?';
  conn.query(sql, [index], function(err, result, fields) {
    if (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
    } else {
      res.send(result);
    }
  });
});

app.post('/introSave', function(req, res) {
  var who = req.body.who;
  var editName = req.body.name;
  var editIntro = req.body.introduce;
  console.log(editIntro);

  var idIndex = 0;
  if (who == 'his')
    idIndex = 1;
  else if (who == 'her')
    idIndex = 2;

  var sql;
  var param;
  if (req.body.photo == ""){
    sql = "UPDATE we SET name=?, introduce=? WHERE id=?";
    param = [editName, editIntro, idIndex];
  }else{
    var editUrl = "images/"+req.body.photo;
    sql = "UPDATE we SET name=?, introduce=?, photoUrl=? WHERE id=?";
    param = [editName, editIntro, editUrl, idIndex];
  }
  console.log(sql);
  console.log(param);
  conn.query(sql, param, function(err, result, status){
    if (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
    } else {
      res.redirect('editIntro.html')
    }
  });
});

app.post('/commentRead', function(req, res) {
  var sql = 'SELECT * FROM comment';
  conn.query(sql, function(err, rows, fields) {
    if (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
    } else {
      res.send(JSON.stringify(rows));
    }
  });
});

// bodyparser : post 방식으로 전달받은 요청 처리
app.post('/commentSave', function(req, res) {
  // http://jsonobject.tistory.com/122
  var id = req.body.id;
  var name = req.body.name;
  var message = req.body.message;

  var sql = "";
  var values = "";
  if (id > 0) {
    sql = 'UPDATE comment SET name=?, message=? WHERE id = ?';
    values = [name, message, id];
  } else {
    sql = 'INSERT INTO comment (name, message) VALUES(?, ?)';
    values = [name, message];
  }
  console.log(values);
  conn.query(sql, values, function(err, result, fields) {
    if (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
    } else {
      if (id > 0)
        editingComment = 0;
      var returnValue = {
        result: 'success'
      };
      res.send(returnValue);
    }
  });
});

app.get('/commentSelected', function(req, res) {
  console.log('- commentSelected' + editingComment);
  if (editingComment == 0) {
    var resultValue = {
      selected: false
    };
    res.send(resultValue);
  } else {
    var sql = 'SELECT * FROM selectedcomment';
    conn.query(sql, function(err, result, fields) {
      var resultValue = {
        selected: true,
        values: JSON.stringify(result[0])
      };
      console.log('- ' + result[0]);
      res.send(resultValue);
    });
  }
});

app.get('/commentSelectCancel', function(req, res) {
  console.log('- commentSelectedCancel' + editingComment);
  editingComment = 0;
  var sql = 'Delete * FROM selectedcomment';
  conn.query(sql, function(err, result, fields){
    if (err) {
      console.log('commentEdit error1 ' + err);
    } else {
      res.send(true);
    }
  });
});

app.post('/commentEdit', function(req, res) {
  const id = req.body.id;
  var sql = 'SELECT * FROM comment WHERE id=?';
  conn.query(sql, [id], function(err, result, fields) {
    if (err) {
      console.log('commentEdit error1 ' + err);
    } else {

      var name = result[0].name;
      var message = result[0].message;

      console.log('- commentEdit 1 ' + id+ ' ,'+ name + ' ,' + message);
      // edit 중인 comment가 있는지 확인
      sql = 'SELECT count(*)"total" FROM selectedcomment';
      conn.query(sql, function(err, result2, fields) {
        if (err) {
          console.log('commentEdit error2 ' + err);
        } else {
          var count = result2[0].total;
          console.log('- commentEdit 2 ' + count);
          if (count == 0) {
            sql = 'INSERT INTO selectedcomment (name, message, orgId) VALUES(?, ?, ?)';
            conn.query(sql, [name, message, id], function(err, rows, fields) {
              if(err){
                console.log('- commentEdit 3-1 ' + err);
              } else {
                console.log('- commentEdit 3-1 ' + rows+ ' ,'+JSON.stringify(rows));
                editingComment = 1;
                res.send(JSON.stringify(rows));
              }
            });
          } else if (count == 1) {
            sql = 'UPDATE selectedcomment SET name=?, message=?, orgId=?';
            conn.query(sql, [name, message, id], function(err, rows, fields) {
              console.log('- commentEdit 3-2 ' + count);
              editingComment = 1;
              res.send(JSON.stringify(rows));
            });
          }
        }
      });
    }
  });
});

app.post('/commentDelete', function(req, res) {
  var id = req.body.id;
  var sql = 'DELETE FROM comment WHERE id=?';
  conn.query(sql, [id], function(err, rows, fields) {
    if (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
    } else {
      //console.log(rows);
      //console.log('-------------');
      //console.log(JSON.stringify(rows));
      res.send(JSON.stringify(rows));
    }
  });
});

app.post('/storyRead', function(req, res) {
  var sql = 'SELECT * FROM story';
  conn.query(sql, function(err, rows, fields) {
    if (err) {
      console.log(err);
      res.status(500).send('Internal Server Error');
    } else {
      res.send(JSON.stringify(rows));
    }
  });
});

app.post('/how2goSetting', function(req, res) {
  how2goSrc = req.body.src;
  how2goDst = req.body.dst;
});

app.post('/showhow2go', function(req, res) {
  var source = req.body.source;
  var url = "";
  if (how2goSrc == "" && how2goDst == "") {
    console.log('1');
    url = "http://map.naver.com/index.nhn?slng=126.8966655&slat=37.4830969&stext=&elng=126.9154817&elat=37.5029131&etext=" + source + "&menu=route&pathType=1";
    //url = "http://map.naver.com/index.nhn?stext="+source+"slng=126.8966655&slat=37.4830969&stext=&elng=126.9154817&elat=37.5029131&etext=해군호텔w웨딩홀&menu=route&pathType=1";
  } else {
    console.log('2');
    url = "http://map.naver.com/index.nhn?slng=" + how2goSrc.x + "&slat=" + how2goSrc.y + "&stext=" + source + "&elng=" + how2goDst.x + "&elat=" + how2goDst.y + "&etext=해군호텔W웨딩홀&menu=route&pathType=1";
  }
  res.redirect(url);
});

app.listen(3000, function() {
  console.log('connected 3000 port!');
});


// nodejs에서 ajax 사용 예제
// http://diegochoi.blogspot.kr/2015/10/nodejs-3-express.html

// ajax stringify
// https://github.com/HomoEfficio/dev-tips/blob/master/Request%20Body%EB%A1%9C%20%EB%B3%B4%EB%82%B4%EC%A7%80%EB%8A%94%20JSON%EC%9D%98%20%ED%96%89%EB%B0%A9%20%EB%B6%88%EB%AA%85.md

// ajax 함수별 설명
// http://noritersand.tistory.com/216

// 축하말 등록 시간 갱신 - 데이터베이스에서 설정 가능
// http://egloos.zum.com/entireboy/v/4880357

// 파일 업로드 - multer 사용
// http://jinbroing.tistory.com/127
// http://paraffa.tistory.com/114

// 무료 도메인 생성
// https://blog.lael.be/post/6070
