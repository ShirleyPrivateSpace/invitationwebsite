// db에서 이름과 소개말을 읽어옵니다.
function loadIntro(whose) {
  $.ajax({
    url: '/introRead',
    datatype: 'json',
    type: 'post',
    cache: false,
    data: {
      who: whose
    },
    success: function(result) {
      $('#' + whose + ' .name').text(result[0].name);
      $('#' + whose + ' .introduce').text(result[0].introduce);
      $('#' + whose + ' .imgurl').attr('src', (result[0].photoUrl));
    },
    error: function(err, status) {
      console.log(err + ',' + status);
    }
  });
}

// 페이지 로딩 시 db 읽기
$(document).ready(function() {
  loadIntro('his');
  loadIntro('her');
});

// 수정 버튼 누르면 새창을 띄웁니다.
$('#his .editButton').click(function(event) {
  var editWindow = window.open('editIntro.html', 'his', "width=400, height=350");
  if (editWindow)
    editWindow.focus();
});

$('#her .editButton').click(function(event) {
  var editWindow = window.open('editIntro.html', 'her', "width=400, height=350");
  if (editWindow)
    editWindow.focus();
});

// 게시판 목록 보이기 display:none->block 설정
$("#mobile-nav").click(function(e){
  e.preventDefault()
  $("#nav-menu").toggleClass("open");
});

// toggleClass
// http://findfun.tistory.com/264
