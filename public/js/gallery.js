// 검은 배경 크기 설정
function setImageBgSize(){
  var maskHeight = $(document).height();
  var maskWidth = $(window).width();
  $('#imageBg').css({
    'height': maskHeight,
    'width': maskWidth
  });
}

$('.photo-item img').click(function(event) {
  var src = $(event.target).attr('src');
  var img = '<img src=' + src + ' style="max-width:100%; width:auto; max-height:100%;" alt="확대이미지" title="크게보니더이쁨"/>';
  $('#imageBox').append(img);
  $('#clickImgSection').show();

  $('html, body').animate({
    scrollTop: 0
  }, 'slow'); // 스크롤 최상단 이동
});

$('#close').click(function(event) {
  var img = $('#imageBox').find('img');
  if (img)
    $(img).remove();
  $('#clickImgSection').hide();
});

$('#zoom').click(function(event) {
  var img = $('#imageBox').find('img');
  if (img) {
    $(img).css('max-height', "");
    $(img).css('height', 'auto');
  }
});

$(window).resize(function() {
  setImageBgSize();
});

// 이미지 풍선말 추가
$(document).ready(function() {

  var img = $('.photo-item').find('img');
  img.attr({
    alt: "이미지",
    title: "클릭해서 크게보세요"
  });

  setImageBgSize();
});

// 게시판 목록 보이기 display:none->block 설정
$("#mobile-nav").click(function(e){
  console.log('1');
  e.preventDefault()
  $("#nav-menu").toggleClass("open");
});

// 검은레이어 위에 창 띄우기
// http://vucket.com/index.php/topic/view/65
// 검은배경 투명도
// https://www.cmsfactory.net/node/10796
