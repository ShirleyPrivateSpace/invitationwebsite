// 축하글 입력 form의 내용을 입력했는지 확인
function checkForm() {
  if ($('#commentForm [name=name]').val() == "") {
    alert("이름을 입력해 주세요");
    $('#commentForm [name=name]').focus();
    return false;
  }
  if ($('#commentForm [name=message]').val() == "") {
    alert("축하말을 입력해 주세요");
    $('#commentForm [name=message]').focus();
    return false;
  }
  //console.log('checkForm - finish');
  return true;
}

// 축하글 입력 form의 내용 초기화
function clearForm() {
  $('#formId').val(0);
  $('#formName').val("");
  $('#formMessage').val("");
}

// 축하글 입력/수정 후 DB 저장
function commentSave() {
  var data = {
    id: $('#formId').val(),
    name: $('#formName').val(),
    message: $('#formMessage').val()
  };
  $.ajax({
    url: '/commentSave',
    dataType: 'json',
    type: 'post',
    cache: false,
    data: data,
    success: function(result) {
      clearForm();
      //console.log('commentSave - success');
    },
    error: function(err, status) {
      console.log('commentSave - err : ' + err + ' ,' + status);
    }
  });
}

// DB에 저장된 모든 축하글 읽어오기
function loadAllComments() {
  $.ajax({
    url: '/commentRead',
    dataType: 'json',
    type: 'post',
    cache: false,
    success: function(result) {
      if (result.length == 0) { // 축하말이 하나도 없는 때
        var add = '<label>이름</label></br>인사말 입니다.'
        $('#comments').append(add);
      } else {
        for (var i = 0; i < result.length; i++) {
          var formatedTime = settingTime(result[i].datetime);
          loadComment(result[i].name, result[i].message, formatedTime, result[i].id);
        }
      }
      //console.log('loadAllComments - success');
    },
    error: function(err, status) {
      console.log('loadAllComments - err : ' + err + ' ,' + status);
    }
  });
}

// 1개의 축하말 html로 구성하기
function loadComment(name, msg, datetime, id) {
  var add = '<label>' + name + '</label><br/>' +
    msg + '<br/>' +
    '<input type="hidden" name="id" value=' + id + '>' +
    datetime +
    '<a onclick="selectEditComment(' + id + ')"> 수정 </a>' +
    ' | ' +
    '<a onclick="deleteComment(' + id + ')"> 삭제</a></br></br>';
  $('#comments').append(add);
}

// 축하글 관련 날짜시간 포맷 구성 시, 0 추가
function addZero(n, digits) {
  var zero = '';
  n = n.toString();

  if (n.length < digits) {
    for (i = 0; i < digits - n.length; i++)
      zero += '0';
  }
  return zero + n;
}

// 축하말 관련 날짜시간 포맷 만들기
function settingTime(time) {
  var timeStr = new Date(time);
  var timeFormat =
    addZero(timeStr.getFullYear(), 4) + '-' +
    addZero(timeStr.getMonth() + 1, 2) + '-' +
    addZero(timeStr.getDate(), 2) + ' ' +
    addZero(timeStr.getHours(), 2) + ':' +
    addZero(timeStr.getMinutes(), 2) + ':' +
    addZero(timeStr.getSeconds(), 2);

  return timeFormat;
}

// 수정을 위해서 선택된 축하말을 form에 출력한다
function printEditForm() {
  $.ajax({
    url: '/commentSelected',
    dataType: 'json',
    type: 'get',
    cache: false,
    success: function(result) {
      var selected = result.selected;
      if (selected) {
        var value = JSON.parse(result.values);
        $("#formName").val(value.name);
        $("#formMessage").val(value.message);
        $("#formId").val(value.orgId);
      }
    },
    error: function(err, status) {
      console.log('printEditForm - err : ' + err + ' ,' + status);
    }
  });
}

// 수정하기 위하여 선택된 축하말을 취소한다
function cancelEdit() {
  $.ajax({
    url: '/commentSelectCancel',
    dataType: 'json',
    type: 'get',
    cache: false,
    error: function(err, status) {
      console.log('cancelEdit - err : ' + err + ',' + status);
    }
  });
}

// 수정을 위하여 선택된 축하말을 확인 및 설정한다
function selectEditComment(id) {
  $.ajax({
    url: '/commentEdit',
    dataType: 'json',
    type: 'post',
    cache: false,
    data: {
      id: id
    },
    success: function(result) {
      //console.log('selectEditComment');
      location.reload();
    },
    error: function(err, status) {
      console.log('selectEditComment - err : ' + err + ',' + status);
    }
  });
}

// 확인하고, 축하말을 삭제한다.
function deleteComment(id) {
  if (confirm('정말 축하댓글을 삭제합니까?')) {
    $.ajax({
      url: '/commentDelete',
      dataType: 'json',
      type: 'post',
      cache: false,
      data: {
        id: id
      },
      success: function(result) {
        clearForm();
        cancelEdit();
        window.location.reload();
      },
      error: function(err, status) {
        console.log(err + ',' + status);
      }
    });
  }
}

// '등록' 버튼에 대한 이벤트 처리
$('#formSubmit').click(function(event) {
  commentSave();
});

// '취소' 버튼에 대한 이벤트 처리
$('#formCancel').click(function(event) {
  clearForm();
  cancelEdit();
});

// 페이지 로딩 시, 축하말 입력 form 수정상태인지 확인, db에 입력된 축하말 읽어서 html로 구성
$(document).ready(function() {
  //console.log('document.ready');
  printEditForm();
  loadAllComments();
});

// 게시판 목록 보이기 display:none->block 설정
$("#mobile-nav").click(function(e){
  e.preventDefault()
  $("#nav-menu").toggleClass("open");
});

// document.ready vs window.load
// http://diaryofgreen.tistory.com/96

// 페이지 새로고침
// http://blog.naver.com/PostView.nhn?blogId=ghkddbguse&logNo=40167268049

// Date 객체 시간 YYYY-MM-DD hh:mm:ss 포맷으로 만들기
// http://blog.naver.com/PostView.nhn?blogId=smart_guy&logNo=100200322125
