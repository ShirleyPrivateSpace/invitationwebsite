// DB에 저장된 이야기 읽어오기
function loadAllStory() {
  $.ajax({
    url: '/storyRead',
    dataType: 'json',
    type: 'post',
    cache: false,
    success: function(result) {
      var currentDivTag = $('Div[class~=story-row]'); // 현재 story-row 클래스를 가진 div
      for (var i = 0; i < result.length; i++) {
        var story = currentDivTag[i].id;
        $('#'+story+' .month-year').text(result[i].year);
        $('#'+story+' .date-only').text(result[i].month);
        $('#'+story+' .content-title').text(result[i].title);
        $('#'+story+' .content1').text(result[i].content1);
        $('#'+story+' .content2').text(result[i].content2);
        $('#'+story+' .story-photo-wrapper img').attr("src",result[i].photoUrl);
        $('#'+story).attr("data-order",result[i].order);
      }
    },
    error: function(err, status) {
      console.log('loadAllStory - err : ' + err + ' ,' + status);
    }
  });
}

$('.story-date').click(function(event){
  var targetDiv = event.target.parentElement.parentElement.parentElement.id;
  var photoDiv = $('#'+targetDiv+' .story-photo-wrapper');
  var dateDiv = $('#'+targetDiv+' .story-date-wrapper');
  var commentDiv = $('#'+targetDiv+' .story-comment-wrapper');

  // 왼 그림 col-md-offset-1
  // 오 그림 col-md-push-6 , col-md-pull-6
  var isLeftPicture = $(photoDiv).hasClass('col-md-offset-1');
  if(isLeftPicture){
    photoDiv.removeClass('col-md-offset-1');
    photoDiv.addClass('col-md-push-6');

    commentDiv.addClass('col-md-pull-6');
  }else{
    photoDiv.removeClass('col-md-push-6');
    photoDiv.addClass('col-md-offset-1');

    commentDiv.removeClass('col-md-pull-6');
  }
});

$(function(){
  $('#our-story').sortable({
    axis:'y',
    cursor:'move',
    opacity:0.5,
    placeholder: 'selected',
    start: function( event, ui ) {
      ui.placeholder.html('여기로 이동');
    },
    update: function(event, ui) {
      //수정사항이 있을 때마다 변환된 순서값을 배열로 표현한다.
      var order = $(this).sortable('toArray', {
  			attribute: 'data-order'
  		});
    }
  });
  $('#our-story').disableSelection();
});

// 페이지 로딩 시, db에 입력된 이야기 읽어오기
$(document).ready(function() {
  //console.log('document.ready');
  loadAllStory();
});

// 게시판 목록 보이기 display:none->block 설정
$("#mobile-nav").click(function(e){
  e.preventDefault()
  $("#nav-menu").toggleClass("open");
});

// div 위치 이동
// https://www.thewordcracker.com/jquery-examples/change-the-order-of-elements-using-jquery/

// 드래그 이동
// https://madplay.github.io/post/2017/12/05/001
// https://jqueryui.com/sortable/#placeholder
