var destination = "";

// 지도탭 선택 시 화면에 활성화된 탭 보여주기
function mapTabControl() {

  $('.tabContent').hide();
  var contentId = $('.current').find('a').attr('contentId');
  $('#' + contentId).show();

  $('#tabMenus div').click(function(event) {
    var selectedTag = event.target.tagName;
    var selectedDivTag = (selectedTag.toString() == 'A') ? $(event.target.parentNode) : $(event.target);
    // A태그일 경우 상위 DIV태그 선택, DIV태그일 경우 그대로 태그 객체

    var currentDivTag = $('Div[class~=current]'); // 현재 current 클래그를 가진 탭
    var isCurrent = $(selectedDivTag).hasClass('current'); // 현재 클릭된 탭이 current를 가졌는지 확인

    if (!isCurrent) {
      var beforeCurrent = $(currentDivTag).find('a').attr('contentId');
      $('#' + beforeCurrent).hide();
      $(currentDivTag).removeClass('current');

      $(selectedDivTag).addClass('current');
      var afterCurrent = $(selectedDivTag).find('a').attr('contentId');
      $('#' + afterCurrent).show();
    }
  });

}

// 다음 지도 가져와서 웨딩홀 보여주기
function useDaumMap() {
  var container = document.getElementById('daumMap'); //지도를 담을 영역의 DOM 레퍼런스
  var options = { //지도를 생성할 때 필요한 기본 옵션
    center: new daum.maps.LatLng(33.450701, 126.570667), //지도의 중심좌표.
    level: 2 //지도의 레벨(확대, 축소 정도)
  };
  var map = new daum.maps.Map(container, options); //지도 객체 생성

  // 주소-좌표 변환 객체를 생성하여, 원하는 주소로 찾아서 마커로 표기함
  var geocoder = new daum.maps.services.Geocoder();
  geocoder.addressSearch('서울 영등포구 가마산로 540', function(result, status) {
    if (status === daum.maps.services.Status.OK) { // 정상적으로 검색이 완료

      // 결과값으로 받은 위치를 마커로 표시합니다
      destination = {
        'LatLngY': result[0].y,
        'LatLngX': result[0].x
      };
      var coords = new daum.maps.LatLng(result[0].y, result[0].x);
      var marker = new daum.maps.Marker({
        map: map,
        position: coords
      });

      // 인포윈도우로 장소에 대한 설명을 표시합니다
      var infowindow = new daum.maps.InfoWindow({
        content: '<div style="width:150px;text-align:center;padding:6px 0;">해군호텔웨딩홀</div>'
      });
      infowindow.open(map, marker);

      // 주소 검색 결과를 지도의 중심으로 설정
      map.setCenter(coords);
    }
  });

  var marker = new daum.maps.Marker({
    // 지도 중심좌표에 마커를 생성
    position: map.getCenter()
  });
  // 지도에 마커를 표시
  marker.setMap(map);
}

// 네이버 제공 함수 : 주소로 위도,경도 찾기
function searchAddressToCoordinate(address) {
  naver.maps.Service.geocode({
    address: address
  }, function(status, response) {
    if (status === naver.maps.Service.Status.ERROR) {
      return alert('Something Wrong!');
    }

    var item = response.result.items[0];
    var point = new naver.maps.Point(item.point.x, item.point.y);
    return point;
  });
}

// 웨딩홀 까지의 길찾기 위해서 출발지 입력 확인
function checkForm() {
  var sourceAddress = $('#How2Go [name=source]').val();
  if (sourceAddress == "") {
    alert("출발지를 정확하게 입력해 주세요");
    $('#commentForm [name=name]').focus();
    return false;
  } else {
    /*
    var source = searchAddressToCoordinate(sourceAddress);
    var destination = searchAddressToCoordinate("서울특별시 영등포구 가마산로 540 군사시설(바다마을아파트),영빈관,본관동");

    $.ajax({
      url : '/how2goSetting',
      dataType : 'json',
      type : 'post',
      cache : false,
      data : {src:source , dst:destination},
      success : function(result){
        console.log(result);
      },
      error : function(err,status){
        console.log(err+','+status);
      }
    });
    */
    return true;
  }
}

$(function() {
  mapTabControl();
  useDaumMap();
});

// 게시판 목록 보이기 display:none->block 설정
$("#mobile-nav").click(function(e){
  e.preventDefault()
  $("#nav-menu").toggleClass("open");
});

// 1) tab control
// http://hkhub.tistory.com/43
// http://victoryan.tistory.com/9 href를 사용하니까 화면이 tabContent를 최상단한 위치로 이동해서 아래로 구현을 변경함
// http://imivory.tistory.com/8
// http://ismydream.tistory.com/98 일찍 알았으면 이 방법을 썼을텐데

// 2) daum map
// http://apis.map.daum.net/web/guide/#step3
// http://apis.map.daum.net/web/documentation/#services_Geocoder

// [class~=value]
// http://findfun.tistory.com/74
