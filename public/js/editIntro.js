// 소개글을 DB에서 얻어옵니다
function loadIntro(whose) {
  $.ajax({
    url: '/introRead',
    datatype: 'json',
    type: 'post',
    cache: false,
    data: {
      who: whose
    },
    success: function(result) {
      console.log(result + ',' );
      $('#photoUrl').text(result[0].photoUrl);
      $('#name').val(result[0].name);
      $('#intro').text(result[0].introduce);
      console.log(result + ',' );
    },
    error: function(err, status) {
      console.log(err + ',' + status);
    }
  });
}

$("input[name=uploadPhoto]").change(function() {
  $('#uploadForm [name=uploadButton]').focus();
  $('#photoStatus').text("");
});

// intercepts the submit event
$("#uploadButton").click(function() {

  if(checkForm()){
    var formdata = new FormData($('#uploadForm')[0]);

    $.ajax({
      type: "POST",
      url: "/uploadImage",
      data: formdata,
      processData: false,
      contentType: false,
      success: function(result)
      {
        $('#uploadPhoto').data("filename", result.filename);
        $("#photoStatus").text("완료");
      },
      error: function(err, status){
        $("#photoStatus").text("실패");
        console.log(err);
      }
    });
    console.log('upload end');
  }
  else{
    return false;
  }
  console.log('upload finish');
});

function checkForm() {
  var image = $('#uploadPhoto')[0].files[0];
  console.log('checkForm'+image);
  if (image == null || image == undefined){
    alert("사진을 선택 후, 업로드하세요");
    $('#uploadForm [name=uploadPhoto]').focus();
    return false;
  }
  return true;
}

/*
$('#loadPhoto').on('change',function(){
  var image = $('#loadPhoto')[0].files[0];
  if (image != null)
    $('#photoStatus').text('선택 완료');
});
*/
// 페이지 로딩 시 이미 등록된 DB 소개글을 읽어옵니다
$(window).load(function(){
  loadIntro(window.name);
});

// 소개글을 저장하고, 소개페이지를 보여줍니다
$('#save').click(function() {
  $.ajax({
    url: '/introSave',
    type: 'post',
    cache: false,
    data: {
      who: window.name,
      name: $('#name').val(),
      introduce: $('#intro').val(),
      photo: $('#uploadPhoto').data("filename")
    },
    beforeSend: function(){
      var bSetPhoto = $('#uploadPhoto').val() != "";
      var bNotUploaded = $('#photoStatus').text() == "";
      if (bSetPhoto && bNotUploaded){
        alert('선택한 사진을 업로드 하세요');
        return false;
      }
      return true;
    },
    success: function(result) {
      window.opener.top.location.href="we.html";
      window.close();
    },
    error: function(err, status) {
      console.log(err + ',' + status);
    }
  });
});

// 게시판 목록 보이기 display:none->block 설정
$("#mobile-nav").click(function(e){
  e.preventDefault()
  $("#nav-menu").toggleClass("open");
});

// 이미지 확장자 검사
// https://developer.mozilla.org/ko/docs/Web/HTML/Element/Input/file

// form과 submit
// https://github.com/tadkim/test/wiki/160811-form-%EA%B4%80%EB%A0%A8-%EB%82%B4%EC%9A%A9-%EC%A0%95%EB%A6%AC

// formdata 이용
// http://kez1994.tistory.com/77

// data() 이용
// http://www.nextree.co.kr/p10155/

// 객체의 속성값 수정
// http://www.everdevel.com/jQuery/attr.php
